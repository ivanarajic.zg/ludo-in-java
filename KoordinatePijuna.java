package covjece.ne.ljuti.se;

public interface KoordinatePijuna {
	public int getx1();
	public void setx1(int k);
	public int getx2();
	public void setx2(int k);
	public int getx3();
	public void setx3(int k);
	public int getx4();
	public void setx4(int k);
	public int gety1();
	public void sety1(int k);
	public int gety2();
	public void sety2(int k);
	public int gety3();
	public void sety3(int k);
	public int gety4();
	public void sety4(int k);
	public int getheight();
	public void setheight(int k);
	public int getwidth();
	public void setwidth(int k);
}
