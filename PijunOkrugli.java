package covjece.ne.ljuti.se;

public class PijunOkrugli extends PijunStore {
	public PijunOkrugli(String s) {
		if(s.equals("crvena")) {
			kp = new KoordinateCrveni();
		} 
		else if(s.equals("plava")) {
			kp = new KoordinatePlavi();
		} 
		else if(s.equals("zelena")) {
			kp = new KoordinateZeleni();
		} 
		else if(s.equals("�uta")) {
			kp = new Koordinate�uti();
		} 
	}
	Pijun napraviPijuna(String s, int x, int y, int height, int width) {
		if(s.equals("crvena")) {
			return new PijunOkrugliCrveni(x, y, height, width);
		} 
		else if(s.equals("plava")) {
			return new PijunOkrugliPlavi(x, y, height, width);
		} 
		else if(s.equals("zelena")) {
			return new PijunOkrugliZeleni(x, y, height, width);
		} 
		else if(s.equals("�uta")) {
			return new PijunOkrugli�uti(x, y, height, width);
		} 
		else 
			return null;
	}
}
