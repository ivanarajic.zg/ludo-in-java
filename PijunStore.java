package covjece.ne.ljuti.se;
import java.awt.*;

public abstract class PijunStore {
	Graphics g;
	Rectangle r;
	KoordinatePijuna kp;
	
	abstract Pijun napraviPijuna(String s, int x, int y, int height, int width);
	public Pijun naručiPijuna(String tip, int x, int y, int height, int width) {
		Pijun pijun = napraviPijuna(tip, x, y, height, width);
		pijun.nacrtaj(g);
		pijun.oblikuj(g, r);
		pijun.obojaj(g);
		return pijun;
	}
}
