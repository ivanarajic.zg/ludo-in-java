package covjece.ne.ljuti.se;

public class PijunKockasti extends PijunStore {
	public PijunKockasti(String s) {
		if(s.equals("crvena")) {
			kp = new KoordinateCrveni();
		} 
		else if(s.equals("plava")) {
			kp = new KoordinatePlavi();
		} 
		else if(s.equals("zelena")) {
			kp = new KoordinateZeleni();
		} 
		else if(s.equals("�uta")) {
			kp = new Koordinate�uti();
		} 
	}
	Pijun napraviPijuna(String s, int x, int y, int height, int width) {
		if(s.equals("crvena")) {
			return new PijunKockastiCrveni(x, y, height, width);
		} 
		else if(s.equals("plava")) {
			return new PijunKockastiPlavi(x, y, height, width);
		} 
		else if(s.equals("zelena")) {
			return new PijunKockastiZeleni(x, y, height, width);
		} 
		else if(s.equals("�uta")) {
			return new PijunKockasti�uti(x, y, height, width);
		} 
		else 
			return null;
	}
}
