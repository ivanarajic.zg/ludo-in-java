package covjece.ne.ljuti.se;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Main {
	JTextField p1, p2, p3, p4;
	JLabel label, l1, l2, l3, l4, l5, br_na_kocki;
	JButton button, kocka;
	JList<String> oblik;
	public int br_oblika;
	static final Color VERY_LIGHT_BLUE = new Color(51, 204, 255);
	
	void go() {
		Scanner slika = new Scanner(System.in);
        System.out.println("Unesite path do slike");
        String path = slika.nextLine();
		
		JFrame frame1 = new JFrame();
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JFrame frame2 = new JFrame();
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        Panel panel = new Panel();
        panel.setBackground(VERY_LIGHT_BLUE);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        MyPanel plo�a = new MyPanel(this, path);
        
        label = new JLabel("Unesite boju s kojom �elite igrati (plava. crvena, zelena, �uta)");
        panel.add(label);
        
        p1 = new JTextField(10);
        p1.addActionListener(new ActionListener() {
        	  public void actionPerformed(ActionEvent e) {
        		  plo�a.boje.add(p1.getText());
        	  }
        });
        p2 = new JTextField(10);
        p2.addActionListener(new ActionListener() {
      	  public void actionPerformed(ActionEvent e) {
      		  plo�a.boje.add(p2.getText());
      	  }
        });
        p3 = new JTextField(10);
        p3.addActionListener(new ActionListener() {
      	  public void actionPerformed(ActionEvent e) {
      		  plo�a.boje.add(p3.getText());
      	  }
        });
        p4 = new JTextField(10);
        p4.addActionListener(new ActionListener() {
      	  public void actionPerformed(ActionEvent e) {
      		  plo�a.boje.add(p4.getText());
      	  }
         });
        
        button = new JButton("Pokreni");
        button.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e){ 
        		plo�a.provjera(br_oblika);
        		frame2.setVisible(true);
        		frame1.dispose();
        	} 
        });
        
        l1 = new JLabel("Igra� 1:");
        l2 = new JLabel("Igra� 2:");
        l3 = new JLabel("Igra� 3:");
        l4 = new JLabel("Igra� 4:");
        l5 = new JLabel("Izaberite oblik pijuna:");
        br_na_kocki = new JLabel();
        
        String ob[] = {"Okrugli", "Kockasti"};
        oblik = new JList(ob);
        oblik.addMouseListener(new MouseListener() {
        	public void mouseClicked(MouseEvent e) {
        		br_oblika = oblik.getMaxSelectionIndex();
        	}
        	public void mousePressed(MouseEvent e) { }
        	public void mouseExited(MouseEvent e) { }
        	public void mouseEntered(MouseEvent e) { }
        	public void mouseReleased(MouseEvent e) { }
        });
        
        kocka = new JButton("Baci kocku");
        kocka.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e){ 
        		int dobiveni_broj = (int)(Math.random() * ((6 - 1) + 1)) + 1;
        		String s = String.valueOf(dobiveni_broj);
        		br_na_kocki.setText(s);
        	} 
        });
        
        panel.add(l1);
        panel.add(p1);
        panel.add(l2);
        panel.add(p2);
        panel.add(l3);
        panel.add(p3);
        panel.add(l4);
        panel.add(p4);
        panel.add(l5);
        panel.add(oblik);
        panel.add(button);
        
        Panel p = new Panel();
        p.setBackground(Color.GREEN);
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
        p.add(kocka);
        p.add(br_na_kocki);
        
        frame2.getContentPane().add(BorderLayout.CENTER, plo�a);
        frame2.getContentPane().add(BorderLayout.EAST, p);
        frame2.setSize(700, 640);
        
        frame1.getContentPane().add(BorderLayout.CENTER, panel);
        frame1.setSize(400, 400);
        frame1.setVisible(true);
	}
	
	public static void main(String[] args) {
		Main m = new Main();
		m.go();
	}
}
