package covjece.ne.ljuti.se;

public class KoordinatePlavi implements KoordinatePijuna {
	int x1, y1, x2, y2, x3, y3, x4, y4, width, height;
	public KoordinatePlavi() {
		this.height = 35;
		this.width = 35;
		this.x1 = 30;
		this.y1 = 25;
		this.x2 = 85;
		this.y2 = 25;
		this.x3 = 30;
		this.y3 = 75;
		this.x4 = 85;
		this.y4 = 75;
	}
	public int getx1() {
		return x1;
	}
	public void setx1(int k) {
		x1 = k;
	}
	public int getx2() {
		return x2;
	}
	public void setx2(int k) {
		x2 = k;
	}
	public int getx3() {
		return x3;
	}
	public void setx3(int k) {
		x3 = k;
	}
	public int getx4() {
		return x4;
	}
	public void setx4(int k) {
		x4 = k;
	}
	public int gety1() {
		return y1;
	}
	public void sety1(int k) {
		y1 = k;
	}
	public int gety2() {
		return y2;
	}
	public void sety2(int k) {
		y2 = k;
	}
	public int gety3() {
		return y3;
	}
	public void sety3(int k) {
		y3 = k;
	}
	public int gety4() {
		return y4;
	}
	public void sety4(int k) {
		y4 = k;
	}
	public int getheight() {
		return height;
	}
	public void setheight(int k) {
		height = k;
	}
	public int getwidth() {
		return width;
	}
	public void setwidth(int k) {
		width = k;
	}
}

