package covjece.ne.ljuti.se;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.util.*;

public class MyPanel extends JPanel {    
    private BufferedImage image;
    ArrayList<Pijun> pijuni = new ArrayList<Pijun>();
    Main main;
    KoordinateCrveni kpc;
    KoordinatePlavi kpp;
    KoordinateZeleni kpz;
    Koordinate�uti kp�;
    ArrayList<String> boje = new ArrayList<String>();
    Pijun pijun1, pijun2, pijun3, pijun4, pijun5, pijun6, pijun7, pijun8, pijun9, pijun10, pijun11, pijun12, pijun13, pijun14, pijun15, pijun16;
    Pijun ozna�eniPijun = null;
    int staraPozicijaTijelaX, staraPozicijaTijelaY;
    int stariXMi�a, stariYMi�a;
    int stariXPijuna, stariYPijuna;

    public MyPanel(Main main, String path) {
    	addMouseListener(new MouseListener() {
    		public void mouseClicked(MouseEvent e) {}
            public void mousePressed(MouseEvent e) {
                if(ozna�eniPijun != null) {
                    ozna�eniPijun.stanje = Stanje.POMICANJE;
                    stariXMi�a = e.getX();
                    stariYMi�a = e.getY();
                    staraPozicijaTijelaX = ozna�eniPijun.tijelo.x;
                    staraPozicijaTijelaY = ozna�eniPijun.tijelo.y;
                    stariXPijuna = ozna�eniPijun.x;
                    stariYPijuna = ozna�eniPijun.y;

                }
            }
            public void mouseReleased(MouseEvent e) { }
            public void mouseEntered(MouseEvent e) { }
            public void mouseExited(MouseEvent e) { }
        });
        addMouseMotionListener(new MouseMotionListener() {
            public void mouseDragged(MouseEvent e) {
                if(ozna�eniPijun != null) {
                	ozna�eniPijun.tijelo.x = staraPozicijaTijelaX + e.getX() - stariXMi�a;
                    ozna�eniPijun.tijelo.y = staraPozicijaTijelaY + e.getY() - stariYMi�a;
                    ozna�eniPijun.x = stariXPijuna + e.getX() - stariXMi�a;
                    ozna�eniPijun.y = stariYPijuna + e.getY() - stariYMi�a;
                    repaint();
                }
            }
            public void mouseMoved(MouseEvent e) {
                if(ozna�eniPijun != null) {
                    ozna�eniPijun.stanje = Stanje.NEAKTIVNA;
                    ozna�eniPijun = null;
                }
                for(Pijun p: pijuni){
                    if( p.unutar(e.getX(), e.getY()) ){
                        p.stanje = Stanje.PODMI�EM;
                        ozna�eniPijun = p;
                        break;
                    }
                }
                repaint();
            }
        });
    	this.main = main;
    	
    	try {                
          image = ImageIO.read(new File(path));
       } 
       catch(IOException ex) {
    	   System.out.println("Slika nije prona�ena!");
       }
    }
    public void provjera(int br) {
        if(br == 0) {
        	for(String s: boje) {
        		PijunStore ps = new PijunOkrugli(s);
        		pijun1 = ps.napraviPijuna(s, ps.kp.getx1(), ps.kp.gety1(), ps.kp.getheight(), ps.kp.getwidth());
    	    	pijun2 = ps.napraviPijuna(s, ps.kp.getx2(), ps.kp.gety2(), ps.kp.getheight(), ps.kp.getwidth());
    	    	pijun3 = ps.napraviPijuna(s, ps.kp.getx3(), ps.kp.gety3(), ps.kp.getheight(), ps.kp.getwidth());
    	    	pijun4 = ps.napraviPijuna(s, ps.kp.getx4(), ps.kp.gety4(), ps.kp.getheight(), ps.kp.getwidth());
    	    	pijuni.add(pijun1);
    	    	pijuni.add(pijun2);
    	    	pijuni.add(pijun3);
    	    	pijuni.add(pijun4);
        	}
        }
        else if(br == 1) {
        	for(String s: boje) {
        		PijunStore ps = new PijunKockasti(s);
        		pijun1 = ps.napraviPijuna(s, ps.kp.getx1(), ps.kp.gety1(), ps.kp.getheight(), ps.kp.getwidth());
    	    	pijun2 = ps.napraviPijuna(s, ps.kp.getx2(), ps.kp.gety2(), ps.kp.getheight(), ps.kp.getwidth());
    	    	pijun3 = ps.napraviPijuna(s, ps.kp.getx3(), ps.kp.gety3(), ps.kp.getheight(), ps.kp.getwidth());
    	    	pijun4 = ps.napraviPijuna(s, ps.kp.getx4(), ps.kp.gety4(), ps.kp.getheight(), ps.kp.getwidth());
    	    	pijuni.add(pijun1);
    	    	pijuni.add(pijun2);
    	    	pijuni.add(pijun3);
    	    	pijuni.add(pijun4);
        	}
        }
    } 
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, this);
        for(Pijun p: pijuni) {
        	p.nacrtaj(g);
        }
    }
}
