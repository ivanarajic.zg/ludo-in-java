package covjece.ne.ljuti.se;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class PijunOkrugliŽuti extends Pijun {
	PijunOkrugliŽuti(int x, int y, int height, int width) {
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		tijelo = new Rectangle(x, y, height, width);
	}
	@Override
	public void obojaj(Graphics g) {
		g.setColor(Color.YELLOW);
	}
	@Override
	public boolean unutar(int xm, int ym) {
		return ((xm - x) * (xm - x) + (ym - y) * (ym - y) <= height * height);
	}
}
