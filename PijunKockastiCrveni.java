package covjece.ne.ljuti.se;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class PijunKockastiCrveni extends Pijun {
	PijunKockastiCrveni(int x, int y, int height, int width) {
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		tijelo = new Rectangle(x, y, height, width);
	}
	public void oblikuj(Graphics g, Rectangle r) {
	   g.fillRect(r.x, r.y, r.width, r.height);
	}
	public void obojaj(Graphics g) {
		g.setColor(Color.RED);
	}
	public boolean unutar(int xm, int ym) {
		return Math.abs(xm - x) <= height && Math.abs(ym - y) <= height;
	}
}
